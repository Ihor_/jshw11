
let flag = 0;

const calculate = {
    operand1 : "",
    sign : "",
    operand2 : "",
    rez : "",
    mem: ""
}



document.querySelector(".keys").addEventListener("click", (e) => {
    if(validate(/\d/, e.target.value)){
        flag = 0;
        calculate.operand1 += e.target.value;
        if (calculate.operand1 && calculate.operand2 && calculate.operand1){

            document.getElementsByClassName("button orange")[0].disabled = false;
        }
        if (calculate.operand1 === '0'){
            document.getElementsByClassName("button black")[10].disabled = true;
            document.getElementsByClassName("button black")[9].disabled = true;
            calculate.operand1 += ".";
        }else{
            document.getElementsByClassName("button black")[9].disabled = false;
        };
        show(calculate.operand1);
    }else if(validate(/^[-/+*]$/, e.target.value)){
        flag = 0;
        if (calculate.sign === e.target.value){
            calc ();
            show(calculate.rez);
        }else if (calculate.sign !== e.target.value && calculate.sign !== ""){
            calc ();
            show(calculate.rez);
        };
        calculate.sign = e.target.value;
        calculate.operand2 = JSON.parse(JSON.stringify(calculate.operand1));
        calculate.operand1 = '';
        document.getElementsByClassName("button black")[10].disabled = false;
    }else if (validate(/^=$/, e.target.value)){
        flag = 0;
        calc();
        document.getElementsByClassName("button black")[10]
        .disabled = false;
        show(calculate.rez);
    }else if (validate(/^C$/, e.target.value)){
        flag = 0;
        Object.keys(calculate).forEach(element => calculate[element] = '');
        document.getElementsByClassName("button black")[10].disabled = false;
        document.getElementsByClassName("button black")[9].disabled = false;
        show(calculate.operand1);
        document.getElementById('m').textContent = '';


    }else if (validate(/^.$/, e.target.value)){
        flag = 0;
        if (calculate.operand1){

            calculate.operand1 += e.target.value;
            show(calculate.operand1);
            document.getElementsByClassName("button black")[10].disabled = true;
        };
    }else if (validate(/m\+/), e.target.value){

        if (e.target.value === "m-"){
            document.getElementById('m').textContent = '';
            calculate.mem = '';
        }else if(e.target.value === "m+"){
            if (calculate.operand1 !== ""){
                
                calculate.mem = parseFloat(calculate.operand1);
             
                document.getElementById('m').textContent = 'm';

            }else if(calculate.operand1 === "" && calculate.operand2 !== "" && calculate.sign !==""){
                
                calculate.mem = parseFloat(calculate.operand2);
                
                document.getElementById('m').textContent = 'm';

            }else if(calculate.operand1 === "" && calculate.operand2 === "" && calculate.sign ==="" && calculate.rez != ""){
                
                calculate.mem = parseFloat(calculate.rez);
               
                document.getElementById('m').textContent = 'm';
                
            }
            
        }else if (e.target.value === "mrc" && calculate.mem){
            if (flag !== 0){
                flag = 0;
                document.getElementById('m').textContent = '';
                return calculate.mem = '';
                
            }else{
                flag++;
                calculate.operand1 = calculate.mem;
                show(calculate.mem);
            }
            
        };
    };
    
});

function show (v) {
    const d = document.querySelector(".display input");

    d.value = v;
};

const validate = (r, v) => r.test(v);


function calc (){
    calculate.operand1 = parseFloat(calculate.operand1);
    calculate.operand2 = parseFloat(calculate.operand2);

    if (calculate.operand1 && calculate.operand2 && calculate.sign && calculate.sign){
        switch(calculate.sign){
            case '-':
                calculate.rez = calculate.operand2 - calculate.operand1;
            break;
            case '/':
                calculate.rez = calculate.operand2 / calculate.operand1;
            break;
            case '+':
                calculate.rez = calculate.operand2 + calculate.operand1;
            break;
            case '*':
                calculate.rez = calculate.operand2 * calculate.operand1;
            break;
        };   
    }else if(calculate.operand1 && calculate.sign && calculate.rez){
        switch(calculate.sign){
            case '-':
                calculate.rez -= calculate.operand1;
            break;
            case '/':
                calculate.rez /= calculate.operand1;
            break;
            case '+':
                calculate.rez += calculate.operand1;
            break;
            case '*':
                calculate.rez *= calculate.operand1;
            break;
        }; 
    }
    calculate.operand1 = '';
    calculate.sign = '';
    calculate.operand2 = '';
};